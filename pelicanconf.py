#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
from subprocess import Popen, PIPE

AUTHOR = 'Association Rebouteuse-DK'
SITENAME = 'Rebouteuse'
SITEURL = 'https://www.rebouteuse.eu'

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
FEED_RSS="feeds/rss.xml"

# Blogroll
LINKS = (
    ('DEBIAN', 'L\'organisation Debian', 'https://debian.org/'),
    ('EMMABUNTÜS','Emmabuntüs','https://emmabuntus.org/'),
    ('FREEDUC','Distribution éducative Freeduc', 'https://usb.freeduc.org'),
)

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)
SOCIAL = None

DEFAULT_PAGINATION = 20

THEME = 'theme-rebouteuse'

PAGE_PATHS = ['images/favicon.png']
STATIC_PATHS = ['images','data','pages', 'videos']
EXTRA_PATH_METADATA = {
    'images/favicon.ico': {'path': 'favicon.ico'},
    'images/valid-xhtml10.png': {'path': 'valid-xhtml10.png'},
    'images/vcss.png': {'path': 'vcss.png'},
}

READERS = {'html': None}

# adds the icon of w3c validator at the bottom of pages enriching base.html
W3C_VALIDATOR = True

SLUGIFY_SOURCE = 'basename'

ARTICLE_URL = 'articles/{date:%Y}/{date:%b}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = 'articles/{date:%Y}/{date:%b}/{date:%d}/{slug}/index.html'
ARTICLE_ORDER_BY = 'reversed-modified'

PAGE_URL = 'pages/{slug}/'
PAGE_SAVE_AS = 'pages/{slug}/index.html'

# if the hostame is "georges" RELATIVE_URLS will be False
RELATIVE_URLS = Popen("hostname", shell=True, stdout=PIPE, stderr=PIPE).communicate()[0].decode("utf-8")!="georges"
