from django import forms

class ReboutageForm(forms.Form):
    rdv = forms.DateTimeField(
        label = "Date et heure de la réunion",
        widget = forms.TextInput(
            attrs = {"placeholder": "AAAA-MM-JJ hh:mm"}
        )
    )
    machines = forms.IntegerField(label = "Nombre de machines à donner")
    max_inscrits = forms.IntegerField(label = "Maximum de visiteurs")
    
