from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.http import HttpResponse, JsonResponse
from django.contrib.auth import authenticate, logout
from django.utils.html import mark_safe
from django.views.decorators.csrf import requires_csrf_token
from django.utils import timezone
import pytz, os, re
from subprocess import call, Popen, PIPE
from html import escape

from .models import *
from .forms import *

def rootdir(request):
    result = ""
    if '127.0.0.1' in request.META.get("HTTP_HOST",""):
        result="reboutelib/"
    return result

# Create your views here.
def index(request):
    return render(request, "index.html", {
        "rootdir": rootdir(request),
        #"grrenviron": os.environ,
        #"grrmeta": request.META,
    })

def login(request):
    user = request.user
    if user.is_anonymous:
        user = None
    error = ""
    username = request.POST.get("username", "")
    password = request.POST.get("password", "")
    username1 = request.POST.get("username1", "")
    password1 = request.POST.get("password1", "")
    inscription = request.POST.get("inscription", "")
    if inscription:
        cmd = 'ldapsearch -D "cn={u},cn=Users,dc=lycee,dc=jb" -w {p} -p 1389 -h localhost -b "dc=lycee,dc=jb" "cn={u}"'.format(u=username1, p=password1)
        p = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
        out, err = p.communicate()
        # on vérifie dans la connexion LDAP
        out = out.decode("utf-8")
        if "extended LDIF" in out:
            try:
                sn = re.search(r"sn: (.*)", out, re.MULTILINE).group(1)
                givenName = re.search(r"givenName: (.*)", out, re.MULTILINE).group(1)
                new_user = User(username = username1,
                                first_name = givenName, last_name = sn)
                new_user.set_password(password1)
                new_user.save()
                lambda_user = Lambda(user = new_user)
                lambda_user.save()
                user = authenticate(request, username=username1, password=password1)
                user = preciseUser(user)
                preciseLogin(request, user)
                return redirect("/"+rootdir(request)+"resa/planning.html")
            except:
                error = "ERREUR : Impossible de créer l'utilisateur "+ username1 + " peut-être existe-t-il déjà ?"
        else:
            error = "ERREUR : Échec de l'authentification par rapport à l'annuaire du lycée Jean Bart, veuillez recommencer"
    else:
        # ce n'est pas une inscription
        if username and password:
            user = authenticate(request, username=username, password=password)
        user = preciseUser(user)
        if user:
            preciseLogin(request, user)
            if not username:
                username = user.username
            return redirect("/"+rootdir(request)+"resa/planning.html")
        else:
            ### pas d'authentification
            if "username" in request.POST:
                ### mais on avait quand même posté quelque chose
                error="Authentification incorrecte ;\\nrecommencez (peut-être devriez-vous passer à l'inscription ?"
    return render(request, "login.html", {
        "login": username,
        "passwd": password,
        "error": mark_safe(error),
        "rootdir": rootdir(request),
    })

def logout_resa(request):
    logout(request)
    return redirect("/"+rootdir(request))

def planning(request):
    if request.POST:
        ### on a posté une demande de nouveau rendez-vous
        form = ReboutageForm(request.POST)
        if form.is_valid():
            reboutage = Reboutage(
                rdv = form.cleaned_data['rdv'],
                responsable = preciseUser(request.user),
                machines = form.cleaned_data['machines'],
                max_inscrits = form.cleaned_data['max_inscrits'],
            )
            reboutage.save()
            form =   ReboutageForm()
    else:
          form =   ReboutageForm()
    the_class = ""
    if isinstance(preciseUser(request.user), Rebouteuse):
        the_class = "Rebouteuse"
    elif isinstance(preciseUser(request.user), Lambda):
        the_class = "Lambda"
    seances = Reboutage.objects.filter( rdv__gt = timezone.now())
    for s in seances:
        s.inscriptions = Inscription.objects.filter(reboutage = s)
        s.machines_demandees = len(Inscription.objects.filter(reboutage = s, demande_ordinateur=True))
        s.ordinateurs_non_reserves = s.machines - s.machines_demandees
        s.places_libres = s.max_inscrits - len(s.inscriptions)
        if s.ordinateurs_non_reserves > s.places_libres:
            s.ordinateurs_non_reserves = s.places_libres
    return render(request, "planning.html", {
        "seances": seances,
        "the_class": the_class,
        "form_reboutage": form,
        "rootdir": rootdir(request),
        "rebouteuse" : the_class == "Rebouteuse",
    })

def delete_resa(request):
    ident = request.POST.get("id", "")
    try:
        reboutage = Reboutage.objects.filter(id = int(ident))
        if reboutage:
            reboutage[0].delete()
    except:
        pass
    return JsonResponse({"msg": "ok"})

def inscription(request):
    ident = request.POST.get("id", "0")
    username = request.POST.get("username", "")
    computer = request.POST.get("computer", "") == "True"
    commentaire = request.POST.get("commentaire", "")
    dejainscrit = Inscription.objects.filter(reboutage__id = int(ident), personne__username = username)
    try:
        ## on vérifie que l'utilisateur n'est pas déjà inscrit
        if dejainscrit:
            return JsonResponse({"msg": "Erreur, l'inscription existe déjà ; cliquez dans la colonne à droite pour la modifier."})
        ## on peut créer l'inscription car les vérifications
        ## du nombre de places et d'ordinateurs ont été faites en amont
        demandeur = User.objects.filter(username = username)
        reboutage = Reboutage.objects.filter(id = int(ident))
        inscription = Inscription(
            personne = demandeur[0],
            reboutage = reboutage[0],
            demande_ordinateur = computer,
            commentaire = commentaire,
        )
        if inscription:
            inscription.save()
            return JsonResponse({"msg": "ok"})
        else:
            return JsonResponse({"msg": "Erreur, l'inscription n'a pas été possible"})
    except:
        return JsonResponse({"msg": "Erreur, l'inscription n'a pas été possible"})
    
def del_inscription(request):
    ident = request.POST.get("id", "")
    username = request.POST.get("username", "")
    demandeur = User.objects.filter(username = username)[0]
    inscription = Inscription.objects.filter(reboutage__id = int(ident), personne__id = demandeur.id)
    commentaire=""
    if inscription:
        inscription[0].delete()
    return JsonResponse({"msg": "ok"})

