from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth import login

# Create your models here.

class Rebouteuse(models.Model):  
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, primary_key=True,)  
    #other fields here

    def __str__(self):  
          return "%s (statut=rebouteuse/rebouteux)" % self.user

    @property
    def the_class(self):
        return "Rebouteuse"

    @property
    def username(self):
        return self.user.username
    
    @property
    def first_name(self):
        return self.user.first_name
    
    @property
    def last_name(self):
        return self.user.last_name
    

class Lambda(models.Model):  
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, primary_key=True,)  
    #other fields here

    def __str__(self):  
          return "%s (statut=ordinaire)" % self.user

    @property
    def the_class(self):
        return "Lambda"

    @property
    def username(self):
        return self.user.username
    
    @property
    def first_name(self):
        return self.user.first_name
    
    @property
    def last_name(self):
        return self.user.last_name
    

class Reboutage(models.Model):
    rdv = models.DateTimeField()
    responsable = models.ForeignKey(Rebouteuse, on_delete=models.PROTECT)
    machines = models.IntegerField()
    max_inscrits = models.IntegerField(default=6)

    def __str__(self):
        return "Reboutage le {rdvj} à {rdvh} avec {resp}".format(
            rdvj = self.rdv.strftime("%Y-%m-%d"),
            rdvh = self.rdv.strftime("%H:%M"),
            resp = self.responsable.user.username
        )

class Inscription(models.Model):
    personne = models.ForeignKey(
        User, on_delete=models.CASCADE) ## peut-être rebouteuse, peut-être lambda
    reboutage = models.ForeignKey(Reboutage, on_delete=models.CASCADE)
    demande_ordinateur = models.BooleanField(default=True)
    commentaire = models.TextField(default = "", blank = True)

    def __str__(self):
        return "{p} --> {r}".format(
            p = self.personne.username,
            r = self.reboutage,
        )
    
def preciseUser(user):
    """
    Rend un utilisateur plus précis, si possible
    """
    if user:
        rebouteuse = Rebouteuse.objects.filter(user__username = user.username)
        lambdauser = Lambda.objects.filter(user__username = user.username)
        if rebouteuse:
            user=rebouteuse[0]
        if lambdauser:
            user=lambdauser[0]
    return user

def preciseLogin(request, user):
    """
    login pour un utilisateur Django de base, ou une Rebouteuse, ou un Lambda
    """
    if isinstance(user, Lambda) or isinstance(user, Rebouteuse):
        login(request, user.user)
    else:
        login(request, user)
    return
