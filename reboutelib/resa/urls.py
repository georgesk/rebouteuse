from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login.html', views.login, name='login'),
    path('logout.html', views.logout_resa, name='logout'),
    path('planning.html', views.planning, name='planning'),
    path('delete_resa', views.delete_resa, name='delete_resa'),
    path('inscription', views.inscription, name='inscription'),
    path('del_inscription', views.del_inscription, name='del_inscription'),
]
