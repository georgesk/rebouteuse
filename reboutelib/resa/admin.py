from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(Rebouteuse)
admin.site.register(Lambda)
admin.site.register(Reboutage)
admin.site.register(Inscription)
