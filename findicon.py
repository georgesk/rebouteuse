#####################################################################
# this module has been written according to the documentation about #
# icons and themes published by FreeDEstop.org, at:                 #
# https://specifications.freedesktop.org/icon-theme-spec/           #
#  icon-theme-spec-latest.html#directory_layout                     #
#                                                                   #
# usage:                                                            #
#                                                                   #
# from findicon import FindIcon                                     #
# gimp_icon = FindIcon("gimp", 45)                                  #
#                                                                   #
# # gimp_icon becomes: /usr/share/icons/hicolor/48x48/apps/gimp.png #
#####################################################################

from subprocess import check_output
import os, re

class Theme:
    def __init__(self, name = ""):
        if name:
            self.name = name
        else:
            self.name = check_output(
                "gsettings get org.gnome.desktop.interface gtk-theme",
                shell=True).decode("utf-8").strip()
        return

    def subdirs(self):
        result = check_output(
                f"find /usr/share/icons/{self.name} -mindepth 1 -maxdepth 1 -type d",
                shell=True)
        return result.decode("utf-8").split("\n")

    def has_parents(self):
        return False

    def parents(self):
        return []
    
def user_selected_theme():
    """ finds the theme selected by the user"""
    return Theme()

def DirectoryMatchesSize(subdir, size, scale=1):
    m = re.match(r".*/(\d+)x(\d+)$", subdir)
    if m:
        return size == int(m.group(1))
    else:
        return False

def DirectorySizeDistance(subdir, size, scale=1):
    m = re.match(r".*/(\d+)x(\d+)$", subdir)
    if m:
        return abs(size - int(m.group(1)))
    else:
        return 1000 # MAXINT
   

def FindIcon(icon, size, scale=1):
  filename = FindIconHelper(icon, size, scale, user_selected_theme())
  if filename:
        return filename

  filename = FindIconHelper(icon, size, scale, Theme("hicolor"));
  if filename:
        return filename

  return LookupFallbackIcon (icon, size, scale)

def LookupFallbackIcon (icon, size, scale=1):
    return "/usr/share/icons/hicolor/scalable/apps/org.gnome.Settings.svg"


def FindIconHelper(icon, size, scale=1, theme="hicolor"):
    filename = LookupIcon (icon, size, scale, theme)
    if filename:
        return filename

    if theme.has_parents():
        parents = theme.parents()
        for parent in parents:
            filename = FindIconHelper (icon, size, scale, parent)
            if filename:
                return filename
    return ""

def LookupIcon (iconname, size, scale=1, theme="hicolor"):
    for subdir in theme.subdirs():
        for extension in ("png", "svg", "xpm"):
            if DirectoryMatchesSize(subdir, size, scale):
                filename = os.path.join(subdir, "apps", iconname + "." + extension)
                if os.path.exists(filename):
                    return filename
    minimal_size = 1000 # MAXINT
    closest_filename = ""
    for subdir in theme.subdirs():
        for extension in ("png", "svg", "xpm"):
            filename = os.path.join(subdir, "apps", iconname+"."+extension)
            if os.path.exists(filename) and DirectorySizeDistance(subdir, size, scale) < minimal_size:
                closest_filename = filename
                minimal_size = DirectorySizeDistance(subdir, size, scale)
    if closest_filename:
        return closest_filename
    return ""



if __name__ == "__main__":
    icon = FindIcon("gimp", 45)
    print("Found icon : ", icon)
    
