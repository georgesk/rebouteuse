Forum de l'ESS 2021
###################

:date:     2021-09-16
:modified: 2021-09-16
:tags: communication
:category: présentation
:slug: forum-ess-2021
:authors: Georges
:language: fr

Cet article est écrit pour l'atelier participatif
« Contre l'obsolescence programmée, reboutons nos ordinateurs »,
présenté jeudi 21 octobre 2021, au
`Forum de l'ESS à Niort <https://www.forum-ess.fr/?Programme2021Jeudi&facette=listeListeThemaprog2021=Ecologie|listeListeSecteur2021=M|listeListeFormatcontribution=Atelier>`_.


Quelles alternatives numériques aux Gafams ?
============================================

Internet est bien plus que les quelques ogres numériques qui dévorent
nos données personnelles, en nous assurant de leur profond respect !

Une chose est claire : si nous voulons demain créer les richesses numériques
près de chez nous, nous devons prêter moins d'attention à nos « amis »
californiens.

|image0|

Que pensez-vous de la suggestion de Péha, qui a
illustré le site web de « `Dégooglisons Internet <https://degooglisons-internet.org/fr/>`_ » ?

Luttons contre le gaspillage et l'obsolescence informatique programmée
======================================================================

Si nous nous laissons bercer, par les « amis » californiens ou ceux de
l'état de Washington, l'informatique nous permet chaque jour plus et
mieux, mais quand même, à condition de racheter vite le nouveau modèle
en magasin !

|image2|

 - Savez-vous qu'un ordinateur a permis en 1969 de guider un équipage humain
   jusqu'à la Lune ?

 - Savez-vous que `tous les 18 mois <https://fr.wikipedia.org/wiki/Loi_de_Moore>`_,
   on a su faire des ordinateurs deux fois plus rapides que juste avant ?

 - Savez-vous qu'il s'est écoulé plus de 30 fois dix-huit mois depuis le
   premier homme sur la Lune ?

 - Alors, si vous le savez, eh bien vous pourriez peut-être guider
   un **milliard** d'équipages humains, vers la Lune (sortez votre calculette,
   vérifiez)

Pourtant, quelques mois après l'achat, nos ordinateurs deviennent trop
lents !

Pourquoi ?
----------

Il n'y a pas vraiment de bonne raison à ce que des machines si rapides
usent notre patience ... Sinon qu'elles sont fort occupées à faire des choses
que nous ne contrôlons pas, par exemple :

 - faire fonctionner, en tâche de fond, des applications dont nous n'avons
   pas besoin ;
 - utiliser, pour faire la même chose qu'avant, une bibliothèque de logiciels
   plus riche pour les créateurs de programmes, mais plus exigeante en
   ressources ;
 - protéger l'ordinateur contre son propriétaire légitime, en vérifiant
   souvent que les licences sont à jour ;
 - chercher les virus, etc.

Découvrez les logiciels libres
==============================

|image3|

L'écosystème des logiciels libres s'enracine dans les années 1990, à peu
de choses près vers les mêmes dates que sont nés le système Windows et
les premiers « Macs ». Google naissait un peu plus tard.

Un logiciel est libre, à quatre conditions
------------------------------------------

 - Je peux *utiliser* le logiciel pour tout usage, sans discrimination
 - Je peux librement *copier* le logiciel et le *redistribuer*
 - Je peux *comprendre* comment il fonctionne : pour cela, les auteurs
   doivent me fournir le *code source* du logiciel ; je l'examine, ou je
   le fais examiner
 - Je peux redistribuer des version *modifiées* du logiciel

Les conséquences de la liberté
------------------------------

 - Avec les quatre règles du Logiciel Libre, un programme qui ferait des
   choses dont l'utilisateur n'a pas besoin se fera tôt ou tard repérer :
   il sera peu diffusé.

 - Les logiciels libres sont souvent plus *légers* que les logiciels non-libres,
   car ils réutilisent constamment les bibliothèques communes : chaque
   concepteur évite de « réinventer la roue », et une petit nombre de
   composants communs suffit comme infrastructure à un grand nombre de
   logiciels.

 - La sécurité des logiciels libres est forte, quand ceux-ci sont développés
   par une communauté forte : en effet, les erreurs sont plus vite corrigées
   quand plusieurs experts sont autorisés à auditer le code source.

 - Et surtout,
   **il est difficile d'introduire un mécanisme d'obsolescence** dans le code
   d'un programme que tout le monde est en droit de comprendre.

Redonnons vie à nos ordinateurs
===============================

Depuis une dizaine d'années, les élèves du lycée Jean Bart qui étudient
de l'informatique peuvent utiliser un bouquet de logiciels libres,
diffusés sur la `clé USB FREEDUC <https://usb.freeduc.org/jbart.html>`_.
De perfectionnement en perfectionnement,
cette clé USB est devenue plus efficace, et surtout elle est maintenant
auto-réplicable.

La clé USB FREEDUC
------------------

C'est un bloc de logiciels libres, gravé au début d'une clé USB
de seize giga-octets, si possible rapide en lecture et en écriture. Comptez
un budget de dix euros pour une qualité matérielle convenable.

Cette clé est capable de démarrer la plupart des ordinateurs personnels,
en une minute environ. Le disque dur de l'ordinateur n'est pas sollicité
*a priori*, et en fonctionnement normal la clé FREEDUC n'y laisse aucune
trace.

Une quarantaine d'applications y apparaissent pour l'utilisateur (un nombre
plus grand sert dans l'infrastructure), chacun des programmes utilisé est
libre.

Conséquence de la liberté du logiciel : LA RÉPLICATION EST LICITE
-----------------------------------------------------------------

|image4|

Depuis le tout début, la clé FREEDUC a été copiable légalement, en totalité.
À partir des versions diffusées en 2018, la réplication est devenue très
facile, grâce au programme intégré `live-clone`, qui procède à une sorte
de bouturage rapide : le cœur du système est répliqué, mais aucune donnée
personnelle ne fuite.

Typiquement, une clé USB peut en produire trois après cinq minutes ; du
coup, les quatre clés peuvent en produire douze dans les cinq minutes
d'après, etc.

« REBOUTER » un ordinateur
==========================

L'opération « Rebouteuse » a été initiée au lycée Jean Bart au début des
restrictions sociales causées par la pandémie. Nos élèves ont, du jour au
lendemain, dû se débrouiller pour devenir experts en télétravail. Nombreux
n'y sont pas parvenus.

Le lycée a annoncé à tous qu'il était possible de solliciter des prêts de
tablettes informatiques, pour pallier des difficultés matérielles ;
rapidement, il a aussi annoncé des **dons** d'ordinateurs reconditionnés,
auxquels on « redonnait vie » en y installant le paquet FREEDUC, sur le
disque dur : ce qui fonctionne avec une clé USB, fonctionne bien, et un peu
plus vite, avec un disque dur.

Une petite modification du programme `live-clone` a permis d'étendre le
« bouturage » aux disques durs classiques.

Tout cela n'aurait pas été possible sans ...
============================================

L'écosystème des logiciels libres n'apparaît pas comme les champignons,
à la faveur d'une météo clémente. Ces programmes sont le fruit d'efforts
cumulés (et cumulables), de centaines de milliers d'acteurs.

Le système d'exploitation, et son infrastructure, nommés
`GNU <https://www.gnu.org/>`_/`Linux <https://www.kernel.org/>`_,
représentent aujourd'hui quelques cinquante millions de lignes de code ;

La distribution logicielle libre `Debian <https://debian.org>`_,
maintenue par des volontaires,
comprend aujourd'hui quelques soixante mille **paquets logiciels** ;
un paquet compte souvent entre mille et cent mille lignes de code.

Parmi les programmes de l'écosystème Debian, on trouve la paquet
`live-build` qui facilite la construction de clés USB vives comme
FREEDUC. La « recette » de FREEDUC est disponible librement, à l'URL
`<https://salsa.debian.org/georgesk/freeduc-jbart>`_.


Crédits des images
==================

 - Dégooglisons Internet : Péha, 2017, CC BY
 - Un escargot, c'est lent : Par teteria sonnna,  Obukhiv, Ukraine —
   2015-07-02-0668(0), CC BY 2.0,
   https://commons.wikimedia.org/w/index.php?curid=46402886
 - Logiciels libres : composition par René Mérou, CC BY SA
 - GNU et Linux vont peupler la galaxie : image de l'auteur, 2016, CC BY SA

   
.. |image0| image:: {static}/images/Peha-Banquet-CC-By-1920.jpg
	:width: 400 px	    

.. |image2| image:: {static}/images/helix.jpg
	:width: 400px

.. |image3| image:: {static}/images/LL.png
	:width: 400px

.. |image4| image:: {static}/images/fond-mediavif.jpg
	:width: 400px


		
