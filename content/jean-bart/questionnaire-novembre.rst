Un questionnaire, en ligne
##########################

:date:     2020-11-05
:modified: 2020-11-05
:tags: échanges, archive
:category: presentation
:slug: sondage-rebouteuse-nov-2020
:authors: Georges
:language: fr

Un sondage, pour recueillir une image le plus possible précise des
opportunités et des besoins dans le
`lycée Jean Bart <https://www.lyceejeanbart.fr>`_ a été mis en ligne pour
les élèves de tous les niveaux, sauf en classe de seconde, où le formulaire
sera rempli sur papier, en cours de SNT.

|image0|

Si vous n'avez pas eu ce formulaire papier, par exemple à cause d'une absence,
vous pouvez le télécharger en
`cliquant ici <{static}/images/sondage-rebouteuse.pdf>`_.

Après l'avoir rempli, faites-le parvenir au casier de M G. Khaznadar,
en salle des professeurs.

.. |image0| image:: {static}/images/sondage-snap1.png
                 :width:  500px
