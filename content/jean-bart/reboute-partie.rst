Reboutage, mode d'emploi
########################

:date:     2020-11-05
:modified: 2020-11-05
:tags: accueil, méthode
:category: Méthode
:slug: reboute-partie
:authors: Georges
:language: fr

Pour « rebouter » un ordinateur, c'est très simple :
====================================================

|image0|

1. convenir d'un rendez-vous avec une rebouteuse ou un rebouteur ... le contact
   peut se faire grâce à une liste à mettre en place (probablement au bureau
   des surveillants, sous réserve de confirmation).
#. après confirmation du rendez-vous, eh bien il faut venir à la salle où
   se fera la prise en main d'un ordinateur « rebouté ».
#. il y aura quelques minutes d'explication, et il faudra s'inscrire dans
   un registre. Soit on décide de s'engager, pour devenir futur
   rebouteuse/rebouteur, ou encore pour faire partie du réseau d'entraide
   entre élèves, soit il faut prévoir de débourser 10 €. Cela sera consigné
   dans le registre, et on garde le double de la déclaration d'engagement ou
   du reçu du paiement.
#. après avoir « réglé les papiers », on passe à l'action : on doit soi-même
   brancher l'ordinateur, le clavier, etc. Si on ne sait pas faire ça, alors
   ce n'est pas la peine ...
#. la rebouteuse ou le rebouteux montre comment démarrer l'ordinateur avec
   une clé USB Freeduc 2020, qui comporte un système complet et plusieurs
   dizaines d'applications.
#. quand l'ordinateur est démarré, il/elle vous montre à déclencher la copie
   du système vers le disque dur (environ trois minutes).
#. ensuite, c'est redémarrage de l'ordinateur « rebouté », sans la clé USB :
   il doit fonctionner sur son disque dur.
#. enfin, visite « du propriétaire », pour voir où sont les applications puis
   arrêt, empaquetage de l'ordinateur à remporter.

Discussion
==========

Il y aura une discussion à la fin de chaque « Reboute Partie », afin de
répondre aux question urgentes, comme :

- où trouver de l'aide si besoin ?
- comment ajouter plus d'applications sur le disque dur ?
- que signifie concrètement devenir rebouteuse/rebouteur ?
- à partir de quand on devient capable d'aider soi-même d'autres personnes ?
- il n'y a que des jeux « vintage » qui peuvent fonctionner sur un ordinateur
  ancien, mais concrètement, comment on les ajoute ?
- comment ça se passe pour le Wifi, pour l'imprimante, pour la webcam, etc.



.. |image0| image:: {static}/images/reboute-egypte.png
	:width: 500 px	    
