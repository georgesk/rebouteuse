Premier Reboutage au lycée Jean Bart
####################################

:date: 2020-11-15
:modified: 2020-11-15
:tags: accueil, archive
:category: présentation
:slug: reboutage-1
:authors: Georges
:language: fr

On commence petit !
===================

La première réunion de reboutage a concerné des élèves du niveau seconde,
elle s'est déroulée jeudi 12 novembre, et il y avait quatre machines à
donner. En fait, un des participants, qui souhaitait un ordinateur
portable plutôt qu'un modèle de bureau, s'est désisté, à onze heures.

Entre 11 et 12 heures
---------------------

Trois ordinateurs ont été Reboutés, dont deux pour des élèves qui sont
repartis avec, et un qui a été rebouté « pour apprendre », par des
membres de l'organisation Rebouteuse qui sont venus pour cela.

Entre 12 et 13 heures
---------------------

Trois ordinateurs ont été Reboutés, dont un pour une élève qui est
repartie avec, et deux ordinateurs portables, amenés par une participante
de l'organisation Rebouteuse, pour tester le fonctionnement des clés
USB utilisées pour les reboutages, et entrer dans le détail des réglages
possibles.

Les ordinateurs apportés étaient des modèles portables, c'est
donc dommage qu'ils aient été là à 12 heures et pas une heure avant,
ils auraient trouvé preneur.

À noter : un des ordinateurs portables, basé sur un processeur ancien,
n'acceptait pas de fonctionner avec le noyau du
`système Freeduc <https://usb.freeduc.org/freeduc-usb/freeduc-jbart/index.fr.html>`_
installé
dans les clés USB de reboutage actuelles : il faudra donc en recompiler
une nouvelle version, avec le support des processeurs d'il y a quinze
ans.

Un court bilan
==============

- Six ordinateurs reboutés,
- Quatre personnes formées à effectuer des reboutages
- Trois des ordinateurs ont trouvé preneur

On a pu tester les points forts et quelques points faibles de la
méthode de reboutage. C'est à peu près au point, mais encore perfectible.

Bientôt quelques extraits vidéo
===============================

La séance a été filmée par deux personnes, des extraits seront mis en
ligne sous peu.
