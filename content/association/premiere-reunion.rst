Première réunion
################

:date: 2020-10-02
:modified: 2020-10-14
:tags: accueil, archive
:category: présentation
:slug: reunion-fondation
:authors: Georges
:language: fr

La réunion de fondation de l'association REBOUTEUSE se déroule le
jeudi **15 octobre** 2020, de **12 à 13 H**, en **salle Lams**
du Lycée Jean Bart.

.. raw:: html
    
    <h3>Emplacement de la réunion</h3>
    <p>
      La salle Lams se situe à l'extrémité Sud-Ouest du bâtiment
      A, après la grande salle de devoirs surveillés A08.
    </p>
    <iframe id="salleLams"
    title="Plan pour la salle Lams"
    width="480"
    height="350"
    src="/pages/lams.html">
    </iframe>



Au programme de cette réunion de fondation :
============================================


Accueil, rappel des buts de l'association
-----------------------------------------

Si certains élèves ont réservé un sandwich, ils peuvent passer le chercher
avant de venir ; au début, on va rapidement passer en revue les buts
de l'association :

- le premier but, c'est de prolonger la vie utile de nombreux ordinateurs,
  c'est donc une série de *gestes éco-citoyens* ;
- le deuxième but, c'est de rendre disponible plein d'ordinateurs
  gratuitement pour toutes celles, tous ceux qui en auraient besoin ;
- un troisième but, c'est de former une poignée d'élèves au métier
  de « reconditionnement d'ordinateurs » ; pas de panique, il ne s'agit
  pas de travail des enfants ni d'exploitation, c'est moins addictif
  que les jeux vidéo, et on apprend beaucoup plus ;
- un quatrième but, c'est de faciliter l'entraide entre élèves : des
  dizaines de logiciels libres, gratuits (légaux) sont à notre disposition,
  encore faut-il s'aider à les maîtriser.

Peut-être déjà une *start-up* ?
-------------------------------

Un élève de classe de première, qui fait la spécialité informatique,
présentera son idée de création d'un site web, pour faciliter les
prises de contact : on pourra y « donner » des ordinateurs anciens,
et y « chercher » des ordinateurs reconditionnés. Le fonctionnement
du site sera assuré grâce à des micropaiements (peu d'argent, beaucoup
de résultat).

Une démonstration en direct
---------------------------

Ce n'est pas tout d'annoncer qu'on sait prendre un ordinateur fatigué, et
le rendre rapide et efficace, encore faut-il le prouver.

Une méthode « par copie d'image disque » sera présentée en direct,
préparez vos chronomètres ... bien sûr en amont, une clé USB est préparée
assez finement, mais devant le public, on va prouver que cette clé sait
rendre un ordinateur
`opérationnel en moins de cinq minutes <{filename}/mode-emploi/comment-faire.rst>`_ !


Recrutements
------------

L'association aura besoin de communiquer : on aura vite besoin de
volontaires pour animer le site web, et aussi d'expertes/experts en
communication pour placer cette association, modestement mais clairement,
dans les réseaux sociaux majoritaires (Facebook, Twitter, ...)

Communiquer c'est bien, mais si la demande est bien là (comme on l'espère)
eh bien il faudra se retrousser les manches : quelques personnes
devront donc savoir reconditionner les ordinateurs.

Le lycée Jean Bart dispose en ce moment d'ordinateurs anciens, mais ce
stock est limité : on aura besoin de personnes qui sauront trouver des
ordinateurs à reconditionner, venant de commerces, d'entreprises qui
apprécieront de faire un don utile ... encore faut-il les démarcher !
