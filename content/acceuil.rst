Rebouteuse
##########

:date: 2020-09-03
:modified: 2020-10-2
:tags: accueil
:category: présentation
:slug: accueil
:authors: Georges
:language: fr

Rebouteuse désigne une organisation basée au `Lycée Jean Bart`_, qui rassemble
des étudiants capables de remettre en état des ordinateurs pas trop anciens, et
de les mettre à disposition de qui en a besoin.

Les ordinateurs reconditionnés, « refourbis », font des postes de travail
très honorables pour la bureautique et la communication, et sont idéaux pour
les études scolaires et universitaires.

Comme les logiciels distribués sont tous sous licences libres, il est permis
à tout un chacun d'apprendre comment ils fonctionnent, voire de les améliorer.

.. _`Lycée Jean Bart`: https://www.lyceejeanbart.fr

Réhabilitation d'anciens ordinateurs
------------------------------------

Vous avez un ordinateur ancien, qui se traîne ? Confiez-le à REBOUTEUSE,
nous y installons un système Debian, et vous retrouvez une machine stable,
agréable à utiliser, évolutive.

À la date de cet article, un ordinateur
fabriqué en 2003 fonctionne sous Windows 7, et à cause des virus et autres
bizarreries, en fait, il ne fonctionne jamais comme on veut.

Contrairement
à Windows 7, Debian est une distribution maintenue à jour, qui s'adapte aux
machines modestes comme aux plus puissantes, et elle ne souffre pas d'invasion
de virus.

Aide et formation
-----------------

Debian, ça décoiffe. On y retrouve tous les logiciels libres que vous
pouvez connaître sous Windows : LibreOffice, Gimp, Inkscape, Firefox,
et beaucoup d'autres. Tous les besoins sont pris en compte.

Vous voulez utiliser une nouvelle application ? Vous aimeriez mieux
savoir utiliser le traitement de texte ? Un problème d'imprimante ?
Les membres de l'association REBOUTEUSE sont là, ils peuvent vous aider.

Un ordinateur de course, bon marché
-----------------------------------

Faire durer un ordinateur quelques années de plus, lui éviter la
déchetterie alors qu'il fonctionne encore, c'est bon pour la planète,
et ça permet de mieux utiliser son argent.

On peut aussi faire pareil, avec un ordinateur neuf : en 2020, quand
on installe Debian sur un micro-ordinateur moderne, on se trouve avec
un système économe en énergie, silencieux, avec une bonne autonomie
s'il est portable, et avec des performances très intéressantes pour le
travail : démarrage en moins 30 secondes, ouverture des documents
immédiate, etc.

Réunion « fondatrice »
----------------------

Une première réunion est prévue le jeudi 15 octobre 2020, de 12 à 13H.
`Cliquez ici pour en savoir plus <{filename}/association/premiere-reunion.rst>`_
