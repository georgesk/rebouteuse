Démarrage d'un système Freeduc - III
####################################

:date:     2021-02-05
:modified: 2021-02-05
:tags: méthode, vidéo
:category: Technique
:slug: demarrage-apres-reboutage
:authors: Georges
:language: fr
	   
.. raw:: html

	 <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://tube.nocturlab.fr/videos/embed/2a971d51-9865-4610-9226-a4cde987dfb0" frameborder="0" allowfullscreen></iframe>

troisième étape du « reboutage »
--------------------------------

La dernière étape est une vérification que tout se passe bien.

Il ne faut pas oublier d'ôter la clé USB Freeduc, qui ne doit pas participer
à ce démarrage-là. Le système qui prend le contrôle de l'ordinateur est bel
et bien celui qui réside maintenant sur le disque dur.
