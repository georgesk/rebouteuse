Démarrage d'un système Freeduc - II
###################################

:date:     2021-02-06
:modified: 2021-02-06
:tags: méthode, vidéo
:category: Technique
:slug: clonage-disque-dur
:authors: Georges
:language: fr
	   
.. raw:: html

	 <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://tube.nocturlab.fr/videos/embed/0f4fecf1-bc65-47ea-8cf0-a4a9316d79e1" frameborder="0" allowfullscreen></iframe>

Deuxième étape du « reboutage »
-------------------------------

Pour « rebouter » un ordinateur, la deuxième étape consiste à copier le
système Freeduc vers le disque dur de celui-ci.

Cette opération ne se lance que depuis un terminal, en tapant la commande
`sudo live-clone --diskinstall` ; il n'y a pas de possibilité d'en faire autant
en cliquant juste dans les menus. Quand l'application (graphique) est lancée,
il faut confirmer deux fois pour autoriser les données de Freeduc à écraser le système présent sur le disque dur.

