Démarrage d'un système Freeduc - I
##################################

:date:     2021-02-07
:modified: 2021-02-07
:tags: méthode, vidéo
:category: Technique
:slug: demarrage-freeduc-usb
:authors: Georges
:language: fr
	   
.. raw:: html

	 <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://tube.nocturlab.fr/videos/embed/3b7265c6-ab1c-4abf-bc36-b94d39b6969b" frameborder="0" allowfullscreen></iframe>

Première étape du « reboutage »
-------------------------------

Pour « rebouter » un ordinateur, la première étape consiste à démarrer
l'ordinateur par une `clé vive Freeduc`_.

C'est seulement quand celle-ci aura bien démarré ...
- qu'on sait à quel point l'ordinateur est compatible avec le système Freeduc ;
- si l'ordinateur est réactif (à quelle vitesse fonctionnent les applications),  en général, ça va plus vite qu'avec le système précédent ;
- et qu'on peut installer le système Freeduc sur le disque dur local.


.. _`clé vive Freeduc`: https://usb.freeduc.org/freeduc-usb/freeduc-jbart/index.fr.html
