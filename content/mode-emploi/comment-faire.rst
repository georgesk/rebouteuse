« Rebouter » les ordinateurs
############################

:date:     2020-09-04
:modified: 2020-10-23
:tags: méthode
:category: Méthode
:slug: mode-d-emploi
:authors: Georges
:language: fr

|image0|
Il y a plusieurs façon de « rebouter » un ordinateur, le points communs sont :

- uniquement des logiciels libres ; de toute façon, installer des logiciels
  tels que Windows ou Mac OSX ne peut se faire qu'en s'acquittant d'une licence
  au fabricant, et en acceptant des conditions « pas libres-libres » (par
  exemple *on ne doit pas chercher à comprendre* comment ces logiciels
  fonctionnement)
- le système de déploiement tient sur une clé USB, et comme sa licence est
  libre, tout le monde a légalement le droit de la recopier, de la distribuer.
- la rapidité de l'installation : moins de dix minutes, dans tous les cas
  de figures

Quelques méthodes sont détaillées ci-dessous :

a. La méthode utilisée au lycée Jean Bart : `« Rebouter » avec Freeduc Jean Bart`_
#. La méthode utilisée pour Emmabuntüs : `Vidéo : cloner un ordinateur en moins de trois minutes`_
#. La même méthode, rendue plus rapide par un déploiement « Clonezilla » : `Vidéo : cloner un ordinateur en moins de deux minutes`_

« Rebouter » avec Freeduc Jean Bart
===================================

La clé USB utilisée pour le lycée Jean Bart est distribuée sur le site
`https://usb.freeduc.org <https://usb.freeduc.org/jbart.html>`__ : on
télécharge son image et on la transfère sur une clé de 16 giga-octets.

Cette clé USB est une clé vive, si on la démarre sur l'ordinateur-cible,
on peut tout de suite vérifier si la matériel est bien supporté. Cette clé
fournit quelques dizaines de logiciels, dont celui qui permet de cloner la
clé vers d'autres clés, et aussi de « rebouter » un ordinateur.

Fiche méthode : cloner la clé Freeduc Jean Bart, vers d'autres clés USB
-----------------------------------------------------------------------

- démarrer un ordinateur avec la clé USB vive Freeduc Jean Bart ;
- brancher une ou plusieurs clés USB de 16 giga-octets ou plus, sans
  bousculer la clé USB vive en fonctionnement ;
- lancer l'application `live-clone`, soit par le menu
  Administration -> Live-clone, soit depuis un terminal, par la ligne
  de commande : `sudo live-clone`\ ;
- cliquer sur le bouton énorme en haut de la fenêtre, pour débuter le clonage ;
- sélectionner les clés désirées, préciser la taille souhaitée pour une
  partition VFAT (pour des échanges possibles sous Windows et Mac OSX) ;
- passer à la suite de l'assistant de clonage, et terminer, en acceptant
  l'auto-clonage (qui est déjà pré-sélectionné)
- l'avancement du clonage est affiché dans des onglets, un par clé USB ;
- quand tout est terminé, le plus simple consiste à quitter l'application
  par le bouton en haut à droite.

Chacune des clés clonées et équivalente à la clé d'origine, les données
personnelles en moins.


Fiche méthode : « rebouter » un ordinateur
------------------------------------------

- démarrer un ordinateur avec la clé USB vive Freeduc Jean Bart ;
- lancer l'application dans le mode spécial de « reboutage », à partir
  d'un terminal, par la ligne de commande  : `sudo live-clone --diskinstall`\ ;
- cliquer sur le bouton énorme en haut de la fenêtre, pour débuter le
  « reboutage » ;
- cocher les deux cases qui affirment qu'on veut vraiment modifier le contenu
  du disque dur de façon définitive, et en perdant les données qui y étaient
  précédemment, puis valider ;
- l'avancement du « reboutage » est affiché dans un onglet ;
- quand tout est terminé, le plus simple consiste à quitter l'application
  par le bouton en haut à droite.

Le disque dur contient alors un système GNU-Linux identique à celui de la
clé USB, les données personnelles en moins.



Vidéo : cloner un ordinateur en moins de trois minutes
======================================================

.. raw:: html

   <video controls width="500"
	 poster="/videos/Moins-de-trois-minutes-pour-l-installation-d-un-ordinateur.png">
     <source src="/videos/Moins-de-trois-minutes-pour-l-installation-d-un-ordinateur.mp4"
            type="video/mp4">
     Désolé, votre navigateur ne supporte pas le marquage "video".
   </video>

Il s'agit de : « clonage d'Emmabuntüs ».

   Auteur :
     BlablaLinux_
   Durée :
     12 min 53 sec
   Licence :
     CC-By-Nc-Sa
   Aussi disponible sur PeerTube :
     https://video.tedomum.net/videos/watch/ad07108e-2ea9-49cc-9052-8d6dfb71f61b

Vidéo : cloner un ordinateur en moins de deux minutes
=====================================================

.. raw:: html

   <video controls width="500"
   poster="/videos/Moins-de-deux-minutes-pour-l-installation-d-un-ordinateur.png">
   <source src="/videos/Moins-de-deux-minutes-pour-l-installation-d-un-ordinateur.mp4"
   type="video/mp4">
   Désolé, votre navigateur ne supporte pas le marquage "video".
   </video>


Il s'agit de : « clonage de DFiso ».


   Auteur :
     BlablaLinux_
   Durée :
     1 min 47 sec
   Licence :
     CC-By-Nc-Sa
   Aussi disponible sur PeerTube :
     https://video.tedomum.net/videos/watch/5ab607e4-ff49-4ed6-8ae3-2a5b0f1c8f31
 
.. _BlablaLinux: https://blablalinux.be/

.. |image0| image:: {static}/images/ll-logo.png
	:width: 200 px	    
