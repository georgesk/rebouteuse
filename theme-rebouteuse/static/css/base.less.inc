// -*- mode: less-css -*-
/**
 * Upstream-Name: fonts-ebgaramond
 * Source: http://www.georgduffner.at/ebgaramond/
 *
 * Copyright: 2010-2012 Georg Duffner (http://www.georgduffner.at)
 * License: OFL-1.1
 **/
@font-face {
 font-family: "Garamond Sans";
 src: url("EBGaramond08-Regular.woff") format("woff"),
}

body, p, ul li, ol li, td {
  font-family:"Garamond Sans",sans-serif;
  font-size: 12pt;
  margin: 0;
  background: @bleu-sombre1;
  color: @dore1;
}

div.pages ul li, div.data-holder p, div.data-holder ul li {
    background: transparent;
}

div.main-article {
  background: @bleu-sombre2
}

H1,H2,H3,H4,H5,H6 {
  font-family:"Garamond Sans",sans-serif;
  font-weight: 600;
  color:  @dore2;
}

H1 { font-size: 2em; }
H2 { font-size: 1.57em; }
H3 { font-size: 1.23em; }
H4 { font-size: 1.1em; }
H5,H6 { font-size: 1.05em; }
 

.header H1 {
  font-size:3em;
  margin-top: 0.2em;
}

.data-holder {
  margin: 1em auto;
  max-width: 960px;
}

.header, .content, .footer { padding: 1em; }

.header { 
  border-bottom: 1px dotted silver;
}
.footer { 
  
    border-top: 1px dotted silver;
}

.main-article { 
  background-color: @blanc-grise;
  margin-bottom: 0.8em;
  padding: 1em;
}

.main-article H2 {
  font-size: 1.9em;
}
.main-article P  {
  font-size: 1.2em;
}

A { 
  text-decoration: none;
  color: @dore2;
  border-bottom: @anti-dore-transparent 2px dotted;
  border-right: @anti-dore-transparent 2px dotted;
  box-shadow: 0.8em 0.8em 0.6em @anti-dore-transparent;
}

A:hover {
  color: @cuir;
  text-shadow: 2px 2px 10px @ombre-cuir;
}

.footer UL { 
  margin:0px ;
    padding: 0px; 
  list-style-type:none;
}
.footer UL LI {
  display: inline-block;
  margin: 1px;
  padding: 1px;
  
}

UL li.active { 
  border: 1px dotted @blanc-grise2;
  background-color: @blanc-grise;
}


.main-article .read-more {
  font-size: 0.9em;
  letter-spacing: 2px;
  border-bottom: 1px dotted silver;
  margin-bottom: 0.5em;
}

.header-article .read-more,
.sec-article .read-more {
  font-size: 0.9em;
  letter-spacing: 2px;
  border-bottom: 1px dotted silver;
  margin-bottom: 0.5em;
}

ul.tag-list {
  list-style-type: none;
  margin:0px;
  padding: 0px;
}

ul.tag-list LI:hover { 
    background-color: whitesmoke;
}

ul.tag-list LI {
  display: inline-block;
  background-color: white;
  border: 1px dotted silver;
  margin-right: 5px;
  margin-bottom: 5px;
  padding: 3px;
}

div.entry-summary p{
  font-size: 1.2em;
}

div.entry-summary {
  margin-bottom: 1em;
}

section.article P { 
  margin-bottom: 1em;
  text-align: justify;
  -moz-hyphens: auto;
  -webkit-hyphens: auto;
}

.main-article P, .sec-article P {
  text-align: justify;
  -moz-hyphens: auto;
  -webkit-hyphens: auto;
}

.pages {
    background-color: @bleu-sombre3;
    color: white;
    padding: 3px;
}

.pages UL {
  list-style-type: none;
  margin: 0px;
}

.pages UL LI {
  display: inline-block;
  margin: 3px 10px 3px 0px;
}

.pages UL LI:nth-child(1) {
  font-size: 1.4em;
}

.pages UL LI A { color: @dore2; font-weight: 600; text-transform: uppercase;}


.pages UL LI.active {
  background-color: transparent;
  border: 0px solid transparent;
  border-bottom: 1px solid @dore2;
}

#image-block { float: right; }

div.hp-header {
  display:table;
  width:100%;
  height:55%;
  max-height: 55%;
  position:relative;
  background: url('/theme/img/jumping-gnu-background.png') no-repeat center center fixed;
  background-size:cover;

}

div.hp-header-inner {
  display:table;
  width:100%;
  height:30%;
  position:relative;
  background:url('/theme/img/jumping-gnu-background.png') no-repeat center center fixed;
  background-size:cover;

}

div.hp-header-inner  #title-block H1 {
  font-size:4em;
  color:@dore2;
  text-align:center
}


div.page-header {
  display:table-cell;
  vertical-align: middle;
  text-align:center;
}

div#title-block {
  margin:10px;
}

div#title-block H1 {
  font-size: 6em;
  font-weight:900;  
  text-shadow: 2px 2px 10px @ombre-dore;
  margin-bottom: 0px;
}

div#title-block H1 A {
  color: @dore2;
}

div#title-block H1 A:hover {
  text-shadow: 2px 2px 10px black;
}

div#title-block P.blurb {
  font-family:"Garamond Sans",sans-serif;
  color: white;
  font-size: 1.5em;
  text-shadow: 1px 1px 3px black;
     margin-top: 0px;
}


.blog_review, .rev_rating {
  background-color: @blanc-grise1;
  padding: 0.5em;
  text-align: justify;
}

.rev_rating {
  margin-bottom: 1em;
}

.blog_review {
    margin-bottom:1em;
}

.rev_rating  span {
  font-weight: 700;
  color: @rouge-sombre1;
}

.a_cura_di { 
  font-size: 0.9em; 
  font-style: italic;
}


.sec-article {
  margin-bottom: 1em;
}

 .footer {
    background-color: @bleu-sombre3;
    color: @dore2;
  }
  
  .footer A {
    color: @dore2;
    font-weight: bold;
  }
  
 .footer H3 { color: @dore2; margin: 0px;}
 
  .footer .active A { color: @rouge-sombre2;}
  .footer .active { color: @gris-sombre1; } 



@media screen and (max-width: 600px) {

  .content { padding: 0px;}
  div.column > * { margin: 1em;}
  
  div.column.aside {
      background-color: @bleu-sombre2;
      padding: 10px;
  }
  
  .aside UL {
      margin: 0px; 
      padding: 4px;
      list-style: none;
    }
  
    .aside UL LI {
      display: inline-block;
      background-color: white; 
      padding: 4px; border: 1px dotted gray
    }
  
   #title-block,#image-block { margin: 0.2em auto; }
  #title-block { text-align: center; }
  #image-block { float: none; }

div.hp-header {
  display:table;
  width:100%;
  height:100%;
  position:relative;
  background:url('/theme/img/jumping-gnu.png') no-repeat center center fixed;
  background-size:cover;

}

div.hp-header-inner {
  display:table;
  width:100%;
  height:30%;
  position:relative;
  background:url('/theme/img/jumping-gnu.png') no-repeat center center fixed;
  background-size:cover;

}

div.hp-header-inner  #title-block H1 {
  font-size:2.5em;
  color:white;
  text-align:center;
}
  
  
div#title-block H1 {
  font-size: 2.5em;
  font-weight:900;  
  text-shadow: 2px 2px 10px black;
  margin-bottom: 0px;
}

  .content .data-holder {
    margin: 0px;
  }
  
  .main-article  {
    padding: 0.5em;
  }

}

div.pages select {
    background-color: @bleu-sombre2;
    color: @dore1;
}

.blogItem h3.blogMeta {
    background-color: @bleu-sombre3;
    color: @rouge-sombre1;
    padding: 5px 4px;
    text-align: right;
    border: @anti-dore-transparent 2px solid;
    border-radius: 4px;
}

.blogItem h3.blogMeta a {
    color: @rouge-sombre1;
}

#title-block {
    display: table;
    width: 100%;
}

#title-block h1 {
    display: table-cell;
    text-align: center;
    vertical-align: middle;
    height: 230px;
}

#title-block h1 a {
    display: inline-block;
}

dl dd {
    padding-left: 5em;
}
