var cssIndex = 0; // index du thème initial
// Liste des thèmes CSS
var cssThemes = [
    {file: "marine.css", title: "Thème Nautes"},
    {file: "vintage.css", title: "Thème Archaïos"},
    {file: "highcontrast.css", title: "Thème Nyx"},
    {file: "darkcyan.css", title: "Thème Hydro"},
    {file: "ocre.css", title: "Thème Gaïa"},
    {file: "gris-clair.css", title: "Thème Zéphyr"},
];
var nbThemes = cssThemes.length;

/**
 * change le thème actif
 * @param nextIndex l'index du style recherché ; si celui-ci est non
 *        défini, alors ce sera le style suivant, modulo nbThemes.
 **/
function switchCSS(nextIndex){
    var currentTheme = cssThemes[cssIndex]
    if (typeof nextIndex === "undefined"){
	nextIndex = (cssIndex + 1) % nbThemes;
    }
    var nextTheme = cssThemes[nextIndex];
    var currentLink = document.querySelector(
	"head link[href*='"+currentTheme.file+"']"
    );
    var nextLink = document.querySelector(
	"head link[href*='"+nextTheme.file+"']"
    );
    if (currentLink) currentLink.media = "none";
    if (nextLink) nextLink.media = "all";
    cssIndex = nextIndex;
    // conserve la valeur de cssIndex dans un cookie valable 1 jour
    creeCookie("styleCss", cssIndex, 1);
    return {index: cssIndex, theme: nextTheme}
}

/**
 * Initialise le menu sélecteur de thèmes
 * @param@ index l'index du style active venant d'un cookie
 **/
function initThemeSelect(index){
    var ts = document.querySelector("#themeSelect");
    if(index){
	index=parseInt(index);
    } else {
	index = -1;
    }
    for (var i = 0; i < nbThemes; i++){
	var op = document.createElement("option");
	op.value = i;
	op.text = cssThemes[i].title;
	if(i == index){
	    op.selected="selected";
	}
	ts.appendChild(op);
    }
}

/**
 * fonction de rappel pour le menu de sélection des thèmes
 **/
function select_theme(){
    var ts = document.querySelector("#themeSelect");
    var wantedIndex = ts.selectedOptions[0].value;
    switchCSS(wantedIndex);
}

function creeCookie(nom, valeur, jours)
{
    var date = new Date();
    date.setTime(date.getTime()+(jours*24*60*60*1000));
    document.cookie = nom + "=" + valeur + "; expires=" + date.toGMTString();
}

function litCookie(nom)
{
    var name = nom + "=";
    var tousLesCookies = document.cookie.split(';');
    for(var i=0; i<tousLesCookies.length; i++)
    {
        var temp = tousLesCookies[i].trim();
        if (temp.indexOf(name)==0)
            return temp.substring(name.length,temp.length);
    }
    return "";
}

/**
 * Initialisations à lancer après mise en place de l'élément select
 **/
function selectLoaded(){
    // conserve un ancien style à travers le cookie "styleCss"
    var index = litCookie("styleCss");
    // mise en place des options des thèmes CSS
    initThemeSelect(index);
    // ajout (en désactivant) les feuilles de style des thèmes
    // supplémentaires.
    var h = document.querySelector("head");
    var firstlink = document.querySelector("#leTheme");
    var prefix = firstlink.href.replace(/(.*\/).*/, '$1');
    for (var i =1; i < nbThemes; i++){
	var link = document.createElement("link");
	link.rel = "stylesheet"
	link.href = prefix + cssThemes[i].file
	link.media = "none";
	h.appendChild(link);
    }
    if (index){
	switchCSS(parseInt(index));
    }
}
