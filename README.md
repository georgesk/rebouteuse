REBOUTEUSE.EU
=============


le site http://www.rebouteuse.eu est la vitrine d'une organisation
basée à Dunkerque, fondée au [lycée Jean Bart](https://www.lyceejeanbart.fr/).

Cette organisation rassemble des étudiants qui se forment à reconditionner
(refourbir) des ordinateurs pas trop anciens, afin de pouvoir les
mettre à disposition gratuitement à qui en a besoin.

Bien sûr, il s'agit de logiciels libres, dont les licences autorisent
ce type de distribution.
