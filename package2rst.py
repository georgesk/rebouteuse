#! /usr/bin/python3
"""
Création d'un tableau des paquets installés, en syntaxe RST (restructured text)
"""

from subprocess import call, Popen, PIPE
import re

img_url = """\
.. |img{n:04d}| image:: http://screenshots.debian.net/thumbnail/{p}
    :align: middle
    :width: {w}


"""

package_url = """\
.. _{p}: https://packages.debian.org/{distro}/{p}

"""

nonX11pattern = re.compile(r"-daemon|-data|-units|-common|-utils|-base|-gnome|-core|-keyring|-backend|-fuse$|-gtk|-perl|-dev$|-bin$|-tools|-setup|xserver-|-info$|tk8")

class PackageInfo:
    """
    A class to deal with package information

    The constructor defines the following information
    - self._allX11List: known packages which have a tag 'interface::x11'
    - self._installedDict: dictionary <installed package> -> <version>
    - self._x11List: installed packages with a tag 'interface::x11'
    - self._nonX11List: installed packages with no such tag
    """
    def __init__(self):
        self.distro = theDistro()
        # known packages which have a tag 'interface::x11'
        cmd = "debtags grep 'interface::x11' | awk -F : '{print $1}'| uniq"
        p = Popen(cmd, shell=True, stdout=PIPE, stdin=PIPE)
        out, err = p.communicate()
        self._allX11List = out.decode("utf-8").strip().split("\n")
        self._allX11List = [p for p in self._allX11List \
                            if not nonX11pattern.search(p)]
        # dictionary <installed package> -> <version>
        cmd = "dpkg-query -W -f '${binary:Package} ${Version}\n'"
        p = Popen(cmd, shell=True, stdout=PIPE, stdin=PIPE)
        out, err = p.communicate()
        self._installedDict = {}
        for line in out.decode("utf-8").strip().split("\n"):
            package, version = line.split(" ")
            package = package.split(":")[0]
            if package not in self._installedDict:
                self._installedDict[package] = version
        set1 = set(self._allX11List)
        set2 = set(self._installedDict.keys())
        intersection = set1 & set2
        difference = set2 - set1
        #
        self._x11List = sorted(list(intersection))
        self._nonX11List = sorted(list(difference))
        self.rst = ""
        return

    def toRst(self, x11 = True, noX11 = True, screenshots = True):
        """
        returns a table in RST format, with installed packages

        @param x11 whether package with a GUI are considered; True by default
        @param noX11 whether package with no GUI are considered; True by default
        @param screenshots whether screenshots are used; True by default
        """
        if self.rst:
            return self.rst
        n = 1
        lines = []
        images = {}
        links = []
        if x11:
            for p in self._x11List:
                if p in self._installedDict:
                    links.append(img_url.format(n=n, p=p, w=160))
                    image = "|img{n:04d}|".format(n=n)
                    lines.append((p+"_", image, self._installedDict[p]))
                    links.append(package_url.format(p=p, distro=self.distro))
                    n += 1
        if noX11:
            for p in self._nonX11List:
                if p in self._installedDict:
                    lines.append((rstReference(p), "", self._installedDict[p]))
                    links.append(package_url.format(p=p, distro=self.distro))
        self.maxcols = [
            max([len(l[0]) for l in lines]), # max length of 1st column
            max([max([len(ll) for ll in l[1].split("\n")]) for l in lines]),
            max([len(l[2]) for l in lines])  # max length of 3rd column
        ]
        result = ""
        if screenshots:
            for l in lines:
                result += self.drawLine()
                result +="| {} | {} | {} |\n".format(
                    adjustLength(l[0], self.maxcols[0]),
                    adjustLength(l[1], self.maxcols[1]),
                    adjustLength(l[2], self.maxcols[2]),
                )
            result += self.drawLine() + "\n\n"
        else:
            for l in lines:
                result += self.drawLine(screenshots = False)
                result +="| {} | {} |\n".format(
                    adjustLength(l[0], self.maxcols[0]),
                    adjustLength(l[2], self.maxcols[2]),
                )
            result += self.drawLine(screenshots = False) + "\n\n"
        for l in links:
            result += l
        self.rst = result
        return result

    def drawLine(self, screenshots=True):
        """
        draw a separation line in the RSR table
        """
        if screenshots:
            return "+-{}-+-{}-+-{}-+\n".format(
                *[n * "-" for n in self.maxcols]
            )
        else:
            return "+-{}-+-{}-+\n".format(
                *[n * "-" for n in (self.maxcols[0], self.maxcols[2])]
            )
             
        
    def toHtmlTable(self, x11 = True, noX11 = True, screenshots = True):
        """
        returns a table in HTML format, with installed packages

        @param x11 whether package with a GUI are considered; True by default
        @param noX11 whether package with no GUI are considered; True by default
        @param screenshots whether screenshots are used; True by default
        """
        cmd = "pandoc -f rst -t html"
        p = Popen(cmd, shell = True, stdin = PIPE, stdout = PIPE)
        p.stdin.write(self.toRst().encode("utf-8"))
        out, err = p.communicate()
        return out.decode("utf-8").replace("<table>", "<table class='packages'>")

def adjustLength(s, l, align="left"):
    if align == "left":
        return s + (l-len(s)) * " "
    return

def rstReference(p):
    return "`{}`_".format(p)

def theDistro():
    distro = "buster"
    with open("/etc/issue") as infile:
        for l in infile.readlines():
            m = re.search(r"Debian GNU.Linux ([a-z]+)", l)
            if m:
                distro = m.group(1)
    return distro

import sys

def usage():
    print(f"""\
Usage: {sys.argv[0]} <base-filename>

  will create two files, <base-filename>.rst and <base-filename>.html
  which describe packages installed on this Debian system.
""")
    return

if __name__ == "__main__":
    if len(sys.argv) > 1 :
        fname = sys.argv[1]
    else:
        usage()
        fname = "test"
    pinfo = PackageInfo()
    with open(fname+".rst","w") as outfile:
        outfile.write(pinfo.toRst())
    print(f"wrote {fname}.rst")
    with open(fname+".html","w") as outfile:
        outfile.write(pinfo.toHtmlTable())
    print(f"wrote {fname}.html")
    
